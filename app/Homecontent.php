<?php
namespace App;


use Illuminate\Database\Eloquent\Model;


class Homecontent extends Model
{


    public $fillable = ['banner_rtitle','banner_rsubtitle','banner_ltitle','banner_lsubtitle','section1_title','section1_content','section2_title','section2_content','section3_title','section3_content','spalsh_text','section4_content','spalsh2_text'];


}