<?php
namespace App;


use Illuminate\Database\Eloquent\Model;


class Blogs extends Model
{


    public $fillable = ['title','slugs','author','thumbnail','content'];


}