<?php

namespace App\Http\Controllers\fe;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Blogs;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $phonenum = DB::select('SELECT phone FROM accountes where id = 1;')[0]->phone;
        $blogs = Blogs::orderBy('id','ASC')->paginate(5);
        return view('fe.blog', compact('phonenum','blogs'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
    // return view('fe.blog');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function blogview(Request $request)
    {
        $slugs = $request->path();

        $phonenum = DB::select('SELECT phone FROM accountes where id = 1;')[0]->phone;
        $blogs = DB::select('SELECT title,slugs,author,thumbnail,content FROM blogs where slugs = "'. $slugs .'"');
        
        return view('fe.blog_view', compact('phonenum','blogs'));;
    }


}
