<?php

namespace App\Http\Controllers\fe;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class AboutusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imgbg = DB::select('SELECT path FROM banners where id = 2;')[0]->path;
        $phonenum = DB::select('SELECT phone FROM accountes where id = 1;')[0]->phone;
        return view('fe.aboutus', compact('phonenum','imgbg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
       // return view('fe.aboutus');
       $imgbg = DB::select('SELECT path FROM banners where id = 2;')[0]->path;
       $phonenum = DB::select('SELECT phone FROM accountes where id = 1;')[0]->phone;
       $content = DB::select('SELECT * FROM pagecontents where id = 1;');
       $testimonials = DB::select('SELECT * FROM testimonials where page = "about" ORDER BY created_at ASC  LIMIT 1');
        return view('fe.aboutus', compact('phonenum','imgbg','content', 'testimonials'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
