<?php

namespace App\Http\Controllers\fe;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cashmemberoffer;
use App\Banners;
use DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imgbg = DB::select('SELECT path FROM banners where id = 1;')[0]->path;
        $phonenum = DB::select('SELECT phone FROM accountes where id = 1;')[0]->phone;
        $testimonials = DB::select('SELECT * FROM testimonials where page = "home" ORDER BY created_at ASC  LIMIT 3 ');
        $areas = DB::select('SELECT name,slugs FROM areas');
        $contents = DB::select('SELECT * FROM homecontents');
        $blogs1 = DB::select('SELECT title,slugs,thumbnail,content FROM blogs ORDER BY id DESC LIMIT 3');
        $blogs2 = DB::select('SELECT title,slugs,thumbnail,content FROM blogs ORDER BY id ASC LIMIT 3');
        $images = DB::select('SELECT path FROM testimonialsimgs LIMIT 3');
        $videos = DB::select('SELECT frame FROM testimonialsvids LIMIT 2');
        return view('fe.index', compact('phonenum','imgbg', 'testimonials', 'areas', 'blogs1', 'blogs2', 'images', 'videos', 'contents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fe.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);


        Cashmemberoffer::create($request->all());
        return redirect()->route('index')
                        ->with('success','You will recieve a text, call or reply from Brian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    return view('fe.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function belist(Request $request)
    {
       // return view('be.testimonials_main');
        $items = Cashmemberoffer::orderBy('id','ASC')->paginate(5);
        return view('be.cashoffers_main',compact('items'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function searvearea(Request $request)
    {
        // $slugs = $request->path();
        $slugs = explode('/',$request->path());
        $id = $slugs[1];

        $phonenum = DB::select('SELECT phone FROM accountes where id = 1;')[0]->phone;
        $areas = DB::select('SELECT name,slugs,contentmain FROM areas where slugs = "'. $id .'"');
        
        return view('fe.servicearea', compact('phonenum','areas'));;
    }
}
