<?php

namespace App\Http\Controllers\fe;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Howitworksquestions;
use DB;

class HowitworksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $imgbg = DB::select('SELECT path FROM banners where id = 4;')[0]->path;
        $phonenum = DB::select('SELECT phone FROM accountes where id = 1;')[0]->phone;
        $content = DB::select('SELECT * FROM pagecontents where id = 3;');
        $testimonials = DB::select('SELECT * FROM testimonials where page = "works" ORDER BY created_at ASC  LIMIT 1');
        return view('fe.howitworks', compact('phonenum','imgbg','content','testimonials'));
         // return view('fe.howitworks');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('howitworks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required'
        ]);


        Howitworksquestions::create($request->all());
        return redirect()->route('howitworks.index')
                        ->with('success','Great! You have successfully register!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        // $imgbg = DB::select('SELECT path FROM banners where id = 4;')[0]->imgbg;
        // return view('fe.howitworks', compact('imgbg'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function belist(Request $request)
    {
       // return view('be.testimonials_main');
        $items = Howitworksquestions::orderBy('id','ASC')->paginate(5);
        return view('be.questionslist_main',compact('items'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    
}
