<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Testimonials;
use App\Testimonialsimg;
use App\Testimonialsvid;
use \Input as Input;

class TestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       // return view('be.testimonials_main');
        $items = Testimonials::orderBy('id','ASC')->paginate(5);
        return view('be.testimonials_main',compact('items'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('be.testimonials_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'page' => 'required',
            'autor' => 'required',
        ]);

        Testimonials::create($request->all());   
        return redirect()->route('testimonialsmain.index')
                        ->with('success','testimonians created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Testimonials::find($id);
        return view('be.testimonials_edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'page' => 'required',
            'autor' => 'required',
        ]);


        Testimonials::find($id)->update($request->all());
        return redirect()->route('testimonialsmain.index')
                        ->with('success','Testimonial has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Testimonials::find($id)->delete();
        return redirect()->route('testimonialsmain.index')
                        ->with('success','Testimonial has been deleted successfully');
    }


    public function imgstore(Request $request)
    {
        $file = Input::file('path');
        if ($file) {

          $file->move('img', $file->getClientOriginalName());
          $img_data = [
                    "path" => $file->getClientOriginalName(),
                ];
            Testimonialsimg::create($img_data);
        }
        
        $items = Testimonialsimg::orderBy('id','ASC')->paginate(15);
        return view('be.testimonials_image_create',compact('items')); 
    }


     public function videomain(Request $request)
    {
        $items = Testimonialsvid::orderBy('id','ASC')->paginate(5);
        return view('be.testimonials_video_main',compact('items'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    public function videocreate()
    {
        return view('be.testimonials_video_create');
    }

    public function videostore(Request $request)
    { 
         $this->validate($request, [
            'frame' => 'required',
        ]);

        Testimonialsvid::create($request->all());   
        return redirect()->action('be\TestimonialsController@videomain')
                ->with('success','testimonians created successfully');
    }

}
