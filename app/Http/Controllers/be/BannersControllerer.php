<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Input as Input;
use App\Banners;


class BannersControllerer extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        //  $items = Banners::orderBy('id','DESC');
        // return view('be.banners',compact('items'));

        $items = Banners::orderBy('id','ASC')->paginate(15);
        return view('be.banners',compact('items'));
            
         // return view('be.banners');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
                 $this->validate($request, [
                'path' => 'required',
                'page' => 'required',
                 ]); 

                 $file = Input::file('path');

                 $file->move('img', $file->getClientOriginalName());

                  $img_data = [
                    "path" => $file->getClientOriginalName(),
                    "page" => $request->page,
                ];

                 if ($request->page == 'home') {
                    $id = '1';
                    Banners::find($id)->update($img_data);
                    return redirect()->route('bannersmain.index')
                          ->with('success','Banners has been updated successfully');

                 }elseif ($request->page == 'about') {
                    $id = '2';
                    Banners::find($id)->update($img_data);
                    return redirect()->route('bannersmain.index')
                          ->with('success','Banners has been updated successfully');

                 }elseif ($request->page == 'service') {
                    $id = '3';
                    Banners::find($id)->update($img_data);
                    return redirect()->route('bannersmain.index')
                          ->with('success','Banners has been updated successfully');

                 }else {
                   $id = '4';
                    Banners::find($id)->update($img_data);
                    return redirect()->route('bannersmain.index')
                          ->with('success','Banners has been updated successfully');

                 }

                 // $getimageName = $request->path->getClientOriginalExtension();
                 // $request->path->move(public_path('img'), $getimageName);


               // Banners::create($img_data);   
               
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
      return view('be.banners');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('be.banners_update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function upload(){
    if(Input::hasFile('file')){

            $file = Input::file('file');
            $file->move('img', $file->getClientOriginalName());

            if ($file) {
                
                echo "Mabuhay.. nagupload at nagsave na ang image";
                die();
            }
        }

    }

}
