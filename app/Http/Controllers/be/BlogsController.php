<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Blogs;
use \Input as Input;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $items = Blogs::orderBy('id','ASC')->paginate(5);
        return view('be.blog_main',compact('items'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
       // return view('be.blog_main');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('be.blog_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'slugs' => 'required',
            'author' => 'required',
            'thumbnail' => 'required',
        ]);
       // if(Input::hasFile('thumbnail')){

       $file = Input::file('thumbnail');
       $file->move('img', $file->getClientOriginalName());


                  $blog_data = [
                    "title" => $request->title,
                    "author" => $request->author,
                    "content" => $request->content,
                    "thumbnail" => $file->getClientOriginalName(),
                    "slugs" => str_replace(' ', '-', strtolower($request->slugs)),
                ];
      
        Blogs::create($blog_data);   
        return redirect()->route('blogsmain.index')
                        ->with('success','Blog created successfully');
         // }else{
         //     return redirect()->route('blogsmain.index')
         //                ->with('warning','Error in saving. try again later.');
         // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
       return view('be.blog_main');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Blogs::find($id);
        return view('be.blog_edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'slugs' => 'required',
            'author' => 'required',
            'thumbnail' => 'required',
        ]);

        $file = Input::file('thumbnail');
       $file->move('img', $file->getClientOriginalName());


                  $blog_data = [
                    "title" => $request->title,
                    "author" => $request->author,
                    "content" => $request->content,
                    "thumbnail" => $file->getClientOriginalName(),
                    "slugs" => str_replace(' ', '-', strtolower($request->slugs)),
                ];

        Blogs::find($id)->update($blog_data);
        return redirect()->route('blogsmain.index')
                        ->with('success','Blog has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blogs::find($id)->delete();
        return redirect()->route('blogsmain.index')
                        ->with('success','Blog has been deleted successfully');
    }
}
