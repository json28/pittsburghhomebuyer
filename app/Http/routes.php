<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/fe', function () {
//     return view('/index');
// });


//Frontend Routes


Route::resource('ItemCRUD','ItemCRUDController');

Route::resource('/','fe\HomeController');
Route::resource('/howitworks', 'fe\HowitworksController');
Route::resource('/contactus', 'fe\ContactusController');
// 
// Route::get('/', 'fe\HomeController@show');
// Route::get('/howitworks', 'fe\HowitworksController@show');
// Route::get('/contactus', 'fe\ContactusController@show');
Route::get('/aboutus', 'fe\AboutusController@show');
Route::get('/ourservice', 'fe\OurserviceController@show');
Route::get('/blog', 'fe\BlogController@show');
Route::get('/recent', 'fe\RecentController@show');


//Backend Routes
Route::get('/pittsburghadmin', 'be\AdminController@show');
Route::post('/pittsburghadmin', 'be\AdminController@doLogin');
Route::get('/admindashboard', 'be\DashboardController@show');
// Route::get('/bannersmain', 'be\BannersControllerer@show');
// Route::get('/upload', 'be\BannersControllerer@upload');

Route::resource('/blogsmain', 'be\BlogsController');
Route::resource('/testimonialsmain', 'be\TestimonialsController');
Route::resource('/accountsmain', 'be\AccountsController');
Route::resource('/pagescontentmain', 'be\PagescontentController');
Route::resource('/homecontentmain', 'be\HomecontentController');
Route::resource('/bannersmain', 'be\BannersControllerer');
Route::resource('/areassmain', 'be\AreasController');

// Route::post('/testimonialsimg/store', 'be\TestimonialsController@imgstore');

Route::any( '/testimonialsimg', array(
            'as' => 'testimonials-img',
            'uses' => 'be\TestimonialsController@imgstore'
        ));

Route::any( '/testimonialsvid', array(
            'as' => 'testimonials-vidmain',
            'uses' => 'be\TestimonialsController@videomain'
        ));
Route::any( '/testimonialsvidcreate', array(
            'as' => 'testimonials-vidcreate',
            'uses' => 'be\TestimonialsController@videocreate'
        ));

Route::any( '/testimonialsvidstore', array(
            'as' => 'testimonials-vidstore',
            'uses' => 'be\TestimonialsController@videostore'
        ));

//listing
Route::get('/cashofferlist', 'fe\HomeController@belist');
Route::get('/questionlist', 'fe\HowitworksController@belist');
Route::get('/contactuslist', 'fe\ContactusController@belist');
Route::get('Areas/{id}', 'fe\HomeController@searvearea');
Route::get('{blogid}', 'fe\BlogController@blogview');
