<?php
namespace App;


use Illuminate\Database\Eloquent\Model;


class Areas extends Model
{


    public $fillable = ['name','slugs','contentmain','thumbnail','option1_content','option1_thumbnail','option2_content','option2_thumbnail','option3_content','option3_thumbnail'];


}