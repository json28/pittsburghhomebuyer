        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin</a>
            </div>

            <div class="header-right">
            <a href="/pittsburghadmin" class="btn btn-danger" title="Logout"><i class="fa fa-exclamation-circle"></i>Log Out</a>
            </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a class="active-menu" href="/admindashboard"><i class="fa fa-dashboard "></i>Dashboard</a>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-yelp "></i>Pages <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                             <li>
                                <a href="{{ url('/homecontentmain', ['id' => 1]) }}"><i class="fa fa-coffee"></i>Home</a>
                            </li>
                            <li>
                                <a href="{{ url('/pagescontentmain', ['id' => 1]) }}"><i class="fa fa-coffee"></i>About Us</a>
                            </li>
                            <li>
                                <a href="{{ url('/pagescontentmain', ['id' => 2]) }}"><i class="fa fa-coffee"></i>Our Service</a>
                            </li>
                            <li>
                                <a href="{{ url('/pagescontentmain', ['id' => 3]) }}"><i class="fa fa-coffee"></i>How it Works</a>
                            </li>
                             <li>
                                <a href="{{ url('/pagescontentmain', ['id' => 4]) }}"><i class="fa fa-coffee"></i>Recent Tab</a>
                            </li>

                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-desktop "></i>Data Tables <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="/cashofferlist"><i class="fa fa-circle-o"></i>Cash Offers</a>
                            </li>
                             <li>
                                <a href="/questionlist"><i class="fa fa-circle-o"></i>Client Question</a>
                            </li>
                            <li>
                                <a href="/contactuslist"><i class="fa fa-circle-o"></i>Contact Us</a>
                            </li>
                        </ul>
                    </li>
                    <!-- <li>
                        <a href=""><i class="fa fa-flash"></i>Data Tables </a>
                    </li> -->
                    <li>
                        <a href="/bannersmain"><i class="fa fa-flash "></i>Banners </a>
                    </li>
                    <!-- <li>
                        <a href="/testimonialsmain"><i class="fa fa-sign-in "></i>Testimonial</a>
                    </li> -->
                     <li>
                        <a href="#"><i class="fa fa-sign-in "></i>Testimonial <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="/testimonialsmain"><i class="fa fa-circle-o"></i>Writings</a>
                            </li>
                             <li>
                                <a href="/testimonialsimg"><i class="fa fa-circle-o"></i>Images</a>
                            </li>
                            <li>
                                <a href="/testimonialsvid"><i class="fa fa-circle-o"></i>Videos</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="/blogsmain"><i class="fa fa-square-o "></i>Blogs</a>
                    </li>
                    <li>
                        <a href="/accountsmain"><i class="fa fa-bug "></i>Accounts</a>
                    </li>
                    <li>
                        <a href="/areassmain"><i class="fa fa-flash "></i>Areas</a>
                    </li>

                </ul>

            </div>

        </nav>