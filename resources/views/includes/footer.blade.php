    <footer class="py-5 solid-bg toppadding">
      <div class="container pull-center">

          <button class="button"><span>{{ $phonenum }}</span></button>

          <p class="toppadding" style="color: white">We are proudly serving Milwaukee and surrounding areas. Contact us today!</p>

          <div class="row toppadding">
            <div class="col-md-6">
              <img src="">
            </div>
            <div class="col-md-6" style="position: relative">
            <p style="position: absolute; top: 20px; width:100%; color: white;">© Copyright 2016, Pittsburgh Home Buyer | All rights reserved</p></div>
          </div>
        <!-- <p class="m-0 text-center text-white small">Copyright &copy; Your Website 2017</p> -->
      </div>
      <!-- /.container -->
    </footer>