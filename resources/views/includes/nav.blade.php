

  
<div id="Intro" class="intro">
<div class="row p-1 topbarbg">
  <div class="col-md-6 pull-left"  style="padding-left: 10%">
    <span class="text_black"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; Call <u>{{ $phonenum }}</u> or <u>TEXT US</u> with questions!</span>
  </div>
  <div class="col-md-6 pull-right" style="padding-right: 10%">
  <div class="social_media">
    <img src="img/fb.png">
    <img src="img/twi.png">
    <img src="img/gool.png">
    <img src="img/youtu.png">
  </div>
    <!-- <i class="fa fa-facebook" aria-hidden="true"></i>
    <i class="fa fa-twitter" aria-hidden="true"></i>
    <i class="fa fa-google-plus" aria-hidden="true"></i>
    <i class="fa fa-youtube" aria-hidden="true"></i> -->
  </div>
</div>
</div>

    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
      <div class="container">
        <a class="navbar-brand" href="/">
        <!-- <i class="fa fa-home" aria-hidden="true"></i> -->
        <img src="img/logo_final.jpg" width="100">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="/aboutus">About Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/ourservice">Our Service</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/howitworks">How it works</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/blog">Blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="recent">Recent tab project</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/contactus">Contact us</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <script type="text/javascript">
     $(window).scroll(function() {

    if ($(this).scrollTop()>0)
     {
       $(".fixed-top").css("top", "0px !important;");
       $("nav").removeClass("fixed-top-sticky");
        $('.intro').fadeOut();
     }
    else
     {
      
      $("nav").addClass("fixed-top-sticky");
       $(".fixed-top").css("top", "40px !important;");
        $('.intro').fadeIn();
      
     }
 });
</script>