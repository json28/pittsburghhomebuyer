<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Pittsburgh Home Buyers</title>

    <!-- BOOTSTRAP STYLES-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" />
    <!-- FONTAWESOME STYLES-->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}" />
       <!--CUSTOM BASIC STYLES-->
    <link rel="stylesheet" href="{{ asset('css/admin_basic.css') }}" />
    <!--CUSTOM MAIN STYLES-->
    <link rel="stylesheet" href="{{ asset('css/admin_custom.css') }}" />

    <link rel="stylesheet" href="{{ asset('css/prettyPhoto.css') }}" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>