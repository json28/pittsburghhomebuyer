    <div id="footer-sec">
        &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
    </div>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script type="text/javascript" src="{!! asset('js/jquery-1.10.2.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/bootstrap.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/jquery.prettyPhoto.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/jquery.mixitup.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/jquery.metisMenu.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/custom.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/galleryCustom.js') !!}"></script>
    <script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    

</body>
</html>
