<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PittsburghHomeBuyer</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" />

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('css/one-page-wonder.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/animated-button.css') }}" />

  </head>

  <body>

    <!-- Navigation -->
  @include('includes.nav') 
<div class="blog_title toppaddingL">
<div class="row">
<div class="col-md-12 wrapper-gray p-2" >
  <h1 style="padding-left: 5%;padding-top:1%;">Contact Us</h1>
  </div>
  </div>
</div>


<div class="Contact sidepadding">
  <div class="row">
    <div class="col-md-6">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d186573.42106224885!2d-88.10751575157235!3d43.05805689176083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880502d7578b47e7%3A0x445f1922b5417b84!2sMilwaukee%2C+WI%2C+USA!5e0!3m2!1sen!2sin!4v1461047513765" width="600" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
    </div>
    <div class="col-md-6" style="padding-left: 5%">
    {!! Form::open(array('route' => 'contactus.store','method'=>'POST')) !!}
        <div class="row">
          <div class="col-md-6 toppadding">
            <input type="text" name="name" value="" size="40" class="contact-form-container " aria-required="true" aria-invalid="false" placeholder="Your Name">
          </div>
          <div class="col-md-6 toppadding">
            <input type="text" name="phone" value="" size="40" class="contact-form-container " aria-required="true" aria-invalid="false" placeholder="Your Phone">
          </div>

          </div>

          <div class="row toppadding">
            <div class="col-md-12">
            <input type="text" name="email" value="" size="40" class="contact-form-container" aria-required="true" aria-invalid="false" placeholder="Your Email">
            </div>
          </div>

          <div class="row toppadding">
            <div class="col-md-12">
            <input type="text" name="subject" value="" size="40" class="contact-form-container" aria-required="true" aria-invalid="false" placeholder="Your Subject">
            </div>
          </div>

          <div class="row toppadding">
            <div class="col-md-12">
            <textarea name="message" cols="30" rows="5" class="contact-form-container" aria-invalid="false" placeholder="Your Message"></textarea>
            </div>
          </div>

          <input type="submit" class="contact-form-container-submit bottompadding">

          {!! Form::close() !!}

           @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

     @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
      </div>
    </div>




</div>

    




    <!-- Footer -->
  @include('includes.footer') 

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/stickynav.js') }}"></script>

      <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
  $.src='//v2.zopim.com/?3om074MuCoFi8hHNAUK6FLvDYur81qhN';z.t=+new Date;$.
  type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
  </script><script>$zopim( function() {
})</script><!--End of Zendesk Chat Script-->

  </body>

</html>
