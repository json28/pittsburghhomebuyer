<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PittsburghHomeBuyer</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" />

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('css/one-page-wonder.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/animated-button.css') }}" />
    <style type="text/css">
      .bg-gradient
        {
            background: rgba(0, 0, 0, 0) linear-gradient(#ffc23f, #fadc06c7) repeat scroll 0 0;
            border-radius: 5px;
            box-shadow: 1px 2px 2px 0;
            display: inline-block;
            margin-right: 20px;
            padding: 10px;
            text-align: center;
            width: 30%;
        }
        a{
          color: white !important;
        }
    </style>



  </head>

  <body>

    <!-- Navigation -->
  @include('includes.nav') 



    <header class="masthead text-center text-white" style="background: url('../img/paperwork.jpg');">

      <div class="row">
        <div class="phone col-md-12 pullcenter semitoppadding">
          <button class="button" style="vertical-align:middle"><span>{{ $phonenum }}</span></button>
        </div>
      </div>

      <div class="masthead-content">
        <div class="container container-fluid">
            

          <div class="row">

          <div class="col-md-8 pull-left p-2">
          <h1 class="aboutus_title p-3">We buy houses in {{ $areas[0]->name }}</h1>
            <div class="row">
                <div class="col-md-12">
                  <p>Need to sell your house fast? Call us at 414-435-2888. We buy houses in {{ $areas[0]->name }} and neighboring areas. We’ll pay in cash and close in 14 days or less!</p>
                </div>
            </div>
          </div>


          <div class="col-md-4  p-2">

            <div class="row">
              <div class="col-md-12">
                <h3 class="p-2 pull-left">Have Questions?</h3>
              </div>
            </div>

            <div class="row bg__blk home_consult_form">
              <div class="col-md-12 pull-left">
                <h6 class="">Get answers- no obligations!</h6>
                <ul class="standard-arrow" style="font-weight:bold">
                  <li>Talk about your situation</li>
                  <li>Get answers &amp; options</li>
                  <li>Request an immediate offer!</li>
                </ul>
                <div role="form" lang="en-US" dir="ltr">
              <form action="/aboutus/#wpcf7-f788-p5-o1" method="post" novalidate="novalidate">
              <div style="display: none;">
              </div>
              <div>
              <b>Your Name*</b><br>
              <span class="wpcf7-form-control-wrap field1">
              <input type="text" name="field1" value="" size="40" class="form-control" id="consult_field1" aria-required="true" aria-invalid="false"></span>
              </div>
              <div style="margin-top:14px">
              <b>Your Phone*</b><br>
              <span class="wpcf7-form-control-wrap field2">
              <input type="tel" name="field2" value="" size="40" class="form-control" id="consult_field2" aria-required="true" aria-invalid="false"></span>
              </div>

              <div>
              <input type="submit" value="GET ANSWER NOW" class="form-control cta_button">
              </div>

              <div class="cta_under">
              We will contact you as soon as possible to find out how we can meet your needs.
              </div>
              </form>
              </div>
              </div>
            </div>
            
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="ourservice semitoppadding sidepadding toppadding">
        {!!$areas[0]->contentmain !!}
          <hr>
    </div>

<div class="sub_services" style="text-align: center;">
           
          <div class="col-3 bg-gradient">
            <div class="class-title">
              <a href="/{!! $areas[0]->slugs !!}">We Buy Houses For Cash In {{ $areas[0]->name }}</a>
            </div>
          </div>
           
          <div class="col-3 bg-gradient">
            <div class="class-title">
              <a href="/{!! $areas[0]->slugs !!}">Sell House Fast In {{ $areas[0]->name }}</a>
            </div>
          </div>
           
          <div class="col-3 bg-gradient">
            <div class="class-title">
              <a href="/{!! $areas[0]->slugs !!}">We Buy Probate Houses In {{ $areas[0]->name }}</a>
            </div>
          </div>
                  </div>


    <!-- Footer -->
  @include('includes.footer') 

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/stickynav.js') }}"></script>

      <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
  $.src='//v2.zopim.com/?3om074MuCoFi8hHNAUK6FLvDYur81qhN';z.t=+new Date;$.
  type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
  </script><script>$zopim( function() {
})</script><!--End of Zendesk Chat Script-->

  </body>

</html>
