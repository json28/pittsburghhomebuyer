<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PittsburghHomeBuyer</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" />

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('css/one-page-wonder.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/animated-button.css') }}" />

    <style type="text/css">
    
    .ellipsis {
    max-width: 90%;
    height:70px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}

</style>

  </head>

  <body>

    <!-- Navigation -->
  @include('includes.nav') 
<div class="blog_title toppaddingL">
<div class="row">
<div class="col-md-12 wrapper-gray p-2" >
  <h1 style="padding-left: 5%; padding-top:1%;">Blog</h1>
  </div>
  </div>
</div>

<div class="blod-list sidepadding">
<br>

@foreach ($blogs as $blog)
  <div class="row recent_post wrapper-gray">
    <div class="col-md-4 p-2 pull-center">
      <img src="../img/{{ $blog->thumbnail }}">
    </div>
    <div class="col-md-8">
      <h3 class="p-3">{{ $blog->title }}</h3>

      <div class="big" id="content">{!! $blog->content !!}
      </div>
      <br>
      <a href="/{!! $blog->slugs !!}"><p class="yellowshi_textcolor">Details >></p></a>
    </div>
  </div>
  <br>
@endforeach



</div>

    <!-- Footer -->
  @include('includes.footer') 



    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/stickynav.js') }}"></script>

      <script type="text/javascript">
        function cutString(id){    
             var text = document.getElementById(id).innerHTML;         
             var charsToCutTo = 400;
                if(text.length>charsToCutTo){
                    var strShort = "";
                    for(i = 0; i < charsToCutTo; i++){
                        strShort += text[i];
                    }
                    document.getElementById(id).title = "text";
                    document.getElementById(id).innerHTML = strShort + "...";
                }            
             };

        cutString('content'); 
  
</script>

      <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
  $.src='//v2.zopim.com/?3om074MuCoFi8hHNAUK6FLvDYur81qhN';z.t=+new Date;$.
  type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
  </script><script>$zopim( function() {
})</script><!--End of Zendesk Chat Script-->

  </body>

</html>
