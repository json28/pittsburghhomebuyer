<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PittsburghHomeBuyer</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" />

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('css/one-page-wonder.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/animated-button.css') }}" />

  </head>

  <body>

    <!-- Navigation -->
  @include('includes.nav') 
<div class="blog_title toppaddingL">
<div class="row">
<div class="col-md-12 wrapper-gray p-2" >
  <h1 style="padding-left: 5%;padding-top:1%;">{{ $blogs[0]->title }}</h1>
  </div>
  </div>
</div>

<div class="recent sidepadding">
<div class="row">
  <div class="col-md-8 toppadding">
  <i class="fa fa-user"></i> {{ $blogs[0]->author }}
     <hr>  

     {!! $blogs[0]->content !!}

  </div>
  <div class="col-md-4 pull-center" >
  <div class="bgsolid  p-3" style="background: #22343d;">
                <div class="row sidebarsolid" >
              <div class="col-md-12">
                <h3 class="p-2 pull-left text_white pull-center">Have Questions?</h3>
              </div>
            </div>

            <div class="row bg__blk home_consult_form m-1">
              <div class="col-md-12 pull-left">
                <h6 class="text_white pull-center">Get answers- no obligations!</h6>
                <ul class="standard-arrow text_white" style="font-weight:bold">
                  <li>Talk about your situation</li>
                  <li>Get answers &amp; options</li>
                  <li>Request an immediate offer!</li>
                </ul>
                <div role="form" lang="en-US" dir="ltr">
              <form action="/aboutus/#wpcf7-f788-p5-o1" method="post" novalidate="novalidate">
              <div style="display: none;">
              </div>
              <div>
              <b>Your Name*</b><br>
              <span class="wpcf7-form-control-wrap field1">
              <input type="text" name="field1" value="" size="40" class="form-control" id="consult_field1" aria-required="true" aria-invalid="false"></span>
              </div>
              <div style="margin-top:14px">
              <b>Your Phone*</b><br>
              <span class="wpcf7-form-control-wrap field2">
              <input type="tel" name="field2" value="" size="40" class="form-control" id="consult_field2" aria-required="true" aria-invalid="false"></span>
              </div>

              <div>
              <input type="submit" value="GET ANSWER NOW" class="form-control cta_button">
              </div>

              <div class="cta_under text_white">
              We will contact you as soon as possible to find out how we can meet your needs.
              </div>
              </form>
              </div>
              </div>
            </div>

            </div>
  </div>

</div>



</div>

    
    <!-- Footer -->
  @include('includes.footer') 

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/stickynav.js') }}"></script>

      <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
  $.src='//v2.zopim.com/?3om074MuCoFi8hHNAUK6FLvDYur81qhN';z.t=+new Date;$.
  type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
  </script><script>$zopim( function() {
})</script><!--End of Zendesk Chat Script-->

  </body>

</html>
