<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PittsburghHomeBuyer</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" />
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('css/one-page-wonder.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/animated-button.css') }}" />
<style type="text/css">
    .ellipsis {
    max-width: 300px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    }
</style>
  </head>

  <body>

    <!-- Navigation -->
  @include('includes.nav')  



    <header class="masthead text-center text-white" style="background: url('../img/{{ $imgbg }}') no-repeat fixed center;">
      <div class="masthead-content">
        <div class="container container-fluid">
          <div class="row">
          <div class="col-md-6 bg__blk pull-left p-5">
           {!!  $contents[0]->banner_rtitle !!}
         <!--  <h2 class="p-3">We Buy Properties Cash</h2>
          <h4 class="p-3"> AS - IS in Pittsburg and Surroundings Areas</h4>
          <p class="p-3">If your thinking about selling your house please contact us today. We buy CASH and can close under 25 days. No matter your location we are familliar with all Pittsburgh communities. Any shape or condition, we will give you a FAIR CASH offer with 24hrs! No repair or inspection necessary.</p> -->
            <div class="row">
                <div class="col-md-6">
                {!!  $contents[0]->banner_rsubtitle !!}
                  <!-- <ul>
                    <li>No Headaches</li>
                    <li>No Commissions</li>
                    <li>No Repairs</li>
                    <li>No Obligation, just call/text or email</li>
                  </ul> -->
                </div>
                <div class="col-md-6">
                  <button class="button" style="vertical-align:middle"><span>{{ $phonenum }}</span></button>
                </div>
            </div>
          </div>
          <div class="col-md-6 bg__org p-5">
            <!-- <h3 class="p-2">Recieve you fash CASH OFFER</h3>
            <h5>TODAY within 24hrs</h5> -->
             {!!  $contents[0]->banner_ltitle !!}
          {!! Form::open(array('route' => 'store','method'=>'POST')) !!}
            <div class="row ">
              <div class="col-md-6 p-l-5">
              <input class="form-control consult_input" type="text" name="fname" value="" size="40"  placeholder="First Name"></div>
              <!-- {!! Form::text('fname', null, array('placeholder' => 'First Name','class' => 'form-control ')) !!} -->
              <div class="col-md-6 p-l-5">
              <input class="form-control consult_input" type="text" name="lname" value="" size="40"  placeholder="Last Name"></div>
              <!-- {!! Form::text('lname', null, array('placeholder' => 'Last Name','class' => 'form-control')) !!} -->
            </div>
            <div class="row">
              <div class="col-md-6 p-l-5">
              <input class="form-control consult_input" type="text" name="phone" value="" size="40"  placeholder="Phone"></div>
              <!-- {!! Form::text('phone', null, array('placeholder' => 'Phone','class' => 'form-control')) !!} -->
              <div class="col-md-6 p-l-5">
               <!-- {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!} -->
              <input class="form-control consult_input" type="text" name="email" value="" size="40"  placeholder="Email Address"></div>
            </div><br>
            

            <div class="row">
              <div class="col-md-12 leftmargin">
              <!-- <input type="submit" value="Get Cash Offer" class="btn btn-danger percent100width"></div> -->
              <button type="submit" class="button btn btn-danger consult_input"><span>Get Cash Offer</span></button>
              <!-- <p style="text-align: center !important">Someone will reach out to you as soon as possible.</p> -->
               {!!  $contents[0]->banner_lsubtitle !!}
            </div><br>

            </div>
          {!! Form::close() !!}

           @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

     @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

            </div>
          </div>
        </div>
    </header>

    <div class="house-buy pull-center toppadding sidepadding">
      <h1 class="title">{{  $contents[0]->section1_title  }}</h1>
      <div class="sidepadding big">{!!  $contents[0]->section1_content !!}</div>
    </div>


    <div class="house-buy-context pull-left toppadding sidepadding">
    <h3>{{  $contents[0]->section2_title }}</h3>
    <div class="row">
      <div class="col-md-12 p-0"">
      {!!  $contents[0]->section2_content !!}
      </div>

    </div>
    </div>
    <div style="clear:both;"></div>
    <div class="who-choose semitoppadding sidepadding">
      <div class="wrapper-gray p-5">
      <h3 class="pull-lef">Why Choose Us?</h3>
      <div class="row">
          <div class="col-md-6">
            <ul>
              <li>We don’t hassle homeowners or force anyone to sell their home.</li>
              <li>Our quotes are non-obligation. You can accept or refuse.</li>
              <li>There are never any unpleasant surprises with us.</li>
            </ul>
          </div>
          <div class="col-md-6 pull-right">
            <input type="submit" value="Testimonials" class="why_us_button"></div>
          </div>
      </div>
      </div>

      <div class="break-space toppadding sidepadding">
      <h5>{{  $contents[0]->section3_title }}</h5>
      <br>
        <div class="row semitoppadding">
          <!-- <div class="col-md-5" style="padding-right: 0% !important">
            <img src="img/we_buy_bottom.png" class="img-responsive mobile__pull_center">
          </div> -->
          <div class="col-md-12 pull-left">
          {!!  $contents[0]->section3_content !!}
          </div>

        </div>
      </div>

      <div class="sell-your-home toppadding simesidepadding">
      <h3 class="pull-center">Do you need to sell your home for any of these reasons?</h3><br>
      <div class="sidepadding">
      <div class="row">
        <div class="col-sm-4 pull-center sidepadding">
          <i class="change_yellow fa fa-home fa-5x"></i>
          <p class="big">Inherited property</p>
        </div>
        <div class="col-sm-4 pull-center sidepadding">
          <i class="change_yellow fa fa-file-text fa-5x "></i>
          <p class="big">City work orders</p>
        </div>
        <div class="col-sm-4 pull-center sidepadding">
          <i class="change_yellow fa fa-gavel fa-5x  "></i>
          <p class="big">Divorce</p>
        </div>
      </div>
      <br><br>
      <div class="row">
        <div class="col-sm-4 pull-center">
          <i class="change_yellow fa fa-wrench fa-5x "></i>
          <p class="big">Too many repairs</p>
        </div>
        <div class="col-sm-4 pull-center">
          <i class="change_yellow fa fa-fire fa-5x "></i>
          <p class="big">Irresponsible tenants</p>
        </div>
        <div class="col-sm-4 pull-center">
          <i class="change_yellow fa fa-lemon-o fa-5x "></i>
          <p class="big">Divorce</p>
        </div>
      </div>
      </div>

      </div><br><br>
      <div class="semitoppadding sidepadding pull-center">
      <h3>{{  $contents[0]->spalsh_text }}</h3>
      </div>

    <div style="clear:both;"></div>
    <div class="who-choose semitoppadding sidepadding topmargin">
      <div class="wrapper-gray">
      <div class="row p-5">
          <div class="col-md-4 toppadding">
          <!-- <input type="submit" value="(412) 513-7204" class="btn btn-danger percent20width"> -->
            <button class="button" style="vertical-align:middle"><span>{{ $phonenum }}</span></button>
          </div>
          <div class="col-md-8">
          <h5>Prefer to phone us? Call us now—we can always</h5>
          <h5>be reached at {{ $phonenum }}. There are absolutely no obligations</h5>
          <br>
            <div class="row">
              <div class="col-md-4"><h4 class="">Areas We Service</h4></div>
              <div class="col-md-4">
                <!-- <select class="classic" id="area-service" onchange="document.location.href=this.options[this.selectedIndex].value;"> -->

                <select class="classic" id="area-service" onchange="if (this.value) window.location.href=this.value">

                    <option value="">Select Your Area</option>
                   @foreach ($areas as $area)
                   <option value="/Areas/{!! $area->slugs !!}">{!! $area->name !!}</option>
                  @endforeach
                </select>
              </div>
            </div>
            </div>
          </div>
      </div>
      </div>
      <div class="recentpost semitoppadding sidepadding">
      <br>
        <h2 class="pull-center">Recent Posts</h2>
        <br>
        <div class="row">
          @foreach ($blogs1 as $blog1)
          <div class="col-md-4">
            <div class="recent_post pull-center grid__border p-2">
            <img src="img/{!! $blog1->thumbnail !!}">
            <h3>{!! $blog1->title !!}</h3>

            <p class="big ellipsis">{!! strip_tags($blog1->content) !!}[...]</p>
            </div>
          </div>
           @endforeach

        </div>
        <br>
        
      </div>

    </div>

    <div class="clear semitoppadding sidepadding">
      <hr>
    </div>
    
    <div class="tesimonials semitoppadding sidepadding">
    <div class="tesimonials__head">
      <div class="row">
        <div class="col-md-6">
          <h2 class="pull-left">Testimonials</h2>
        </div>
        <div class="col-md-6">
          <a style="background-color:#fcb827; color:#ffffff;" class="pull-right p-2 m-r-2 m-l-2"  title="Testimonials">Testimonials</a>
        </div>
      </div>
    </div>

    <div class="tesimonials__video semitoppadding">
      <div class="row">

         @foreach ($videos as $video)
          <div class="col-md-6">
         <iframe src="{!! $video->frame !!}" width="560" height="315" frameborder="0" allowfullscreen></iframe>
         </div>
           @endforeach
      </div>
    </div>

    <div class="tesimonials__text semitoppadding">
      <div class="row">
      @foreach ($testimonials as $testimonial)
        <div class="col-md-4 wrapper-gray">
          <p class="p-2 big truncate">{!! $testimonial->content !!} </p>
          <p class="pull-right orage__text"><i><span class="yellowshi_textcolor">Read more</span></i></p><br>
          <h5>{{ $testimonial->autor }}</h5>
        </div>
        @endforeach
      </div>
      <br><br>

      <div class="row">
      @foreach ($images as $image)
        <div class="col-md-4 simesidepadding">
          <img src="img/{!! $image->path !!} " class="img-responsive mobile__pull_center">
          <div style="background: #75cdff">
          <!-- <p class="p-2">Billy E. Harley <i> -->
          <!-- <span class="yellowshi_textcolor pull-right">Read More</span></i></p> -->
            
          </div>
        </div>
        @endforeach

      </div>
    </div>

    </div>

    <!-- Footer -->
  @include('includes.footer') 

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/stickynav.js') }}"></script>

      <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
  $.src='//v2.zopim.com/?3om074MuCoFi8hHNAUK6FLvDYur81qhN';z.t=+new Date;$.
  type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
  </script><script>$zopim( function() {
})</script><!--End of Zendesk Chat Script-->



  </body>

</html>
