<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<div align="right">
<!-- <button type="button" class="btn btn-primary">Add Testimonials</button> -->
 <!-- <a class="btn btn-primary" href="{{ route('testimonialsmain.create') }}"> Create New Item</a> -->
</div><br>

<!-- body here -->

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($item, ['method' => 'PATCH','route' => ['areassmain.update', $item->id]]) !!}


            <div class="panel panel-default">
                    <div class="panel-heading" style="background: #fcb827">
                        Area Main Content
                    </div>
                        <div class="panel-body">


                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Name:</strong>
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Slugs:</strong>
                                {!! Form::text('slugs', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Content:</strong>
                                {!! Form::textarea('contentmain', null, array('id'=> 'summary-ckeditor','placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Upload Tumbnail:</strong>
                                <label>Select image to upload:</label>
                                <input type="file" name="thumbnail" id="thumbnail">
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

                <div class="panel panel-default">
                    <div class="panel-heading" style="background: #fcb827">
                        Buy House for Cash Content
                    </div>
                        <div class="panel-body">

                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Content:</strong>
                                {!! Form::textarea('option1_content', null, array('id'=> 'summary-ckeditor','placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Upload Tumbnail:</strong>
                                <label>Select image to upload:</label>
                                <input type="file" name="option1_thumbnail" id="option1_thumbnail">
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            </div>
                        </div>

                    </div>
                </div>
            </div>


                <div class="panel panel-default">
                    <div class="panel-heading" style="background: #fcb827">
                        Sell My House Content
                    </div>
                        <div class="panel-body">

                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Content:</strong>
                                {!! Form::textarea('option2_content', null, array('id'=> 'summary-ckeditor','placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Upload Tumbnail:</strong>
                                <label>Select image to upload:</label>
                                <input type="file" name="option2_thumbnail" id="option2_thumbnail">
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                    <div class="panel-heading" style="background: #fcb827">
                        Buy House Probate Content
                    </div>
                        <div class="panel-body">

                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Content:</strong>
                                {!! Form::textarea('option3_content', null, array('id'=> 'summary-ckeditor','placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Upload Tumbnail:</strong>
                                <label>Select image to upload:</label>
                                <input type="file" name="option3_thumbnail" id="option3_thumbnail">
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    <br><br><br>
<!-- end of body -->
     {!! Form::close() !!}
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
<script type="text/javascript" src="{!! asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}"></script>
<!-- <script src="{{ asset('') }}"></script> -->
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
<!-- footer -->
 @include('includes.admin-foot') 