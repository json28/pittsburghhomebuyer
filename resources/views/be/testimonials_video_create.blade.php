<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<!-- body here -->
<div class="panel panel-default">
                        <div class="panel-heading">
                            Testimonials
                        </div>
                        <div class="panel-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            {!! Form::open(array('route' => 'testimonials-vidstore','method'=>'POST')) !!}
                    <div class="row">


                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>iFrame:</strong>
                                {!! Form::text('frame', null, array('placeholder' => 'https://www.youtube.com/embed/######','class' => 'form-control')) !!}
                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                        </div>


                    </div>
        {!! Form::close() !!}
                    </div>






<!-- end of body -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
<script type="text/javascript" src="{!! asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}"></script>
<!-- <script src="{{ asset('') }}"></script> -->
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
<!-- footer -->
 @include('includes.admin-foot') 