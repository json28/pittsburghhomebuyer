<style type="text/css">
    .ellipsis {
    max-width: 300px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    }
</style>
<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<div align="right">
<!-- <button type="button" class="btn btn-primary" href="{{ route('blogsmain.create') }}">Add Blogs</button> -->
 <a class="btn btn-primary" href="{{ route('blogsmain.create') }}"> Add Blogs</a>
</div><br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('warning'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif
<div class="panel panel-default">
                        <div class="panel-heading">
                            Blogs
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                            
                                        <tr>
                                            <th>Title</th>
                                            <th>Author</th>
                                            <th max-width="120">Content</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach ($items as $key => $item)
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $item->title }}</td>
                                            <td>{{ $item->author }}</td>
                                            <td class="ellipsis">{{ strip_tags($item->content) }}</td>
                                            <td>
                                            <a class="btn btn-primary" href="{{ route('blogsmain.edit',$item->id) }}">Edit</a>
                                            {!! Form::open(['method' => 'DELETE','route' => ['blogsmain.destroy', $item->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                            </td>
                                        </tr>
                                @endforeach
                                    </tbody>
                                </table>
                         {!! $items->render() !!}
                            </div>
                        </div>
                    </div>

<!-- end of body -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

<!-- footer -->
 @include('includes.admin-foot') 
 