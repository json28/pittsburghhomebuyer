<style type="text/css">
    
    .ellipsis {
    max-width: 80px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}
</style>


<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<div align="right">
<!-- <button type="button" class="btn btn-primary">Add Testimonials</button> -->
 <!-- <a class="btn btn-primary" href="{{ route('testimonialsmain.create') }}"> Create New Item</a> -->
</div><br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
<!-- body here -->
<div class="panel panel-default">
                        <div class="panel-heading">
                            Clients Question Datalist
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                            
                                        <tr>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Date</th>
                                        </tr>
                                        @foreach ($items as $key => $item)
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->phone }}</td>
                                            <td>{{ $item->created_at }}</td>
                                        </tr>
                                @endforeach
                                    </tbody>
                                </table>
                         {!! $items->render() !!}
                            </div>
                        </div>
                    </div>


<!-- end of body -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

<!-- footer -->
 @include('includes.admin-foot') 