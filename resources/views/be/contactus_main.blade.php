<style type="text/css">
    
    .ellipsis {
    max-width: 80px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}
</style>


<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<div align="right">
<!-- <button type="button" class="btn btn-primary">Add Testimonials</button> -->
 <!-- <a class="btn btn-primary" href="{{ route('testimonialsmain.create') }}"> Create New Item</a> -->
</div><br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
<!-- body here -->
<div class="panel panel-default">
                        <div class="panel-heading">
                            Contact us Datalist
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                            
                                        <tr>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Subject</th>
                                            <th>Message</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach ($items as $key => $item)
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->phone }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->subject }}</td>
                                            <td class="ellipsis">{{ $item->message }}</td>
                                            <td class="ellipsis">{{ $item->created_at }}</td>
                                            <td>
                                            <!-- <a class="btn btn-info" href="{{ route('ItemCRUD.show',$item->id) }}">Show</a> -->
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#showmodal{{ $item->name }}">
                                              Show
                                            </button>

                                            </td>
                                            
                                        </tr>



                                    <div class="modal fade" id="showmodal{{ $item->name }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Contact us Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                           <b>Name:</b> {{ $item->name }}<br>
                                           <b>Phone:</b> {{ $item->phone }}<br>
                                           <b>Email:</b> {{ $item->email }}<br>
                                           <b>Subject:</b> {{ $item->email }}<br>
                                           <b>Message:</b> {{ $item->message }}<br>

                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

























                                @endforeach
                                    </tbody>
                                </table>
                         {!! $items->render() !!}
                            </div>
                        </div>
                    </div>


<!-- end of body -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->


<!-- Modal -->







<!-- footer -->
 @include('includes.admin-foot') 