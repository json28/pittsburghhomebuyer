<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<!-- body here -->
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
            <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           Accounts
                        </div>
                        <div class="panel-body">
                            {!! Form::model($item, ['method' => 'PATCH','route' => ['accountsmain.update', $item->id]]) !!}
                                    <div class="form-group">
                                         <label>Phone Number</label>
                                        <div class="input-group">
                                              <span class="form-group input-group-btn">
                                                <button class="btn btn-default"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                              </span>
                                          <!-- <input type="text" class="form-control" /> -->
                                          {!! Form::text('phone', null, array('placeholder' => 'Phone Number','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <label>Gmail</label>
                                        <div class="input-group">
                                              <span class="form-group input-group-btn">
                                                <button class="btn btn-default"><i class="fa fa-google-plus" aria-hidden="true"></i></button>
                                              </span>
                                              {!! Form::text('gmail', null, array('placeholder' => 'Gmail Account','class' => 'form-control')) !!}
                                          <!-- <input type="text" class="form-control" /> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <label>Facebook</label>
                                        <div class="input-group">
                                              <span class="form-group input-group-btn">
                                                <button class="btn btn-default"><i class="fa fa-facebook-square" aria-hidden="true"></i></button>
                                              </span>
                                          <!-- <input type="text" class="form-control" /> -->
                                          {!! Form::text('facebook', null, array('placeholder' => 'Facebook URL','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <label>Instagram</label>
                                        <div class="input-group">
                                              <span class="form-group input-group-btn">
                                                <button class="btn btn-default"><i class="fa fa-instagram" aria-hidden="true"></i></button>
                                              </span>
                                          <!-- <input type="text" class="form-control" /> -->
                                          {!! Form::text('instagram', null, array('placeholder' => 'Instagram Account','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <label>Tweeter</label>
                                        <div class="input-group">
                                              <span class="form-group input-group-btn">
                                                <button class="btn btn-default"><i class="fa fa-twitter-square" aria-hidden="true"></i></button>
                                              </span>
                                          <!-- <input type="text" class="form-control" /> -->
                                          {!! Form::text('tweeter', null, array('placeholder' => 'Tweeter Account','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <label>Youtube</label>
                                        <div class="input-group">
                                              <span class="form-group input-group-btn">
                                                <button class="btn btn-default"><i class="fa fa-youtube-play" aria-hidden="true"></i></button>
                                              </span>
                                          <!-- <input type="text" class="form-control" /> -->
                                          {!! Form::text('youtube', null, array('placeholder' => 'Youtube Channel','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    
                                 
                                        <button type="submit" class="btn btn-info">Update </button>

                                   
                            </div>
                        </div>
                    </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-danger">
                        <div class="panel-heading">
                           Google map
                        </div>
                        <div class="panel-body">
                        
                        <div class="form-group">
                                <label>Api Key</label>
                                <!-- <textarea class="form-control" rows="3"></textarea> -->
                                {!! Form::textarea('gmap', null, array('placeholder' => 'googlemap embed code','class' => 'form-control')) !!}
                        </div>
                        <button type="submit" class="btn btn-info">Update </button>
                         {!! Form::close() !!}
                        <hr>

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d186573.42106224885!2d-88.10751575157235!3d43.05805689176083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880502d7578b47e7%3A0x445f1922b5417b84!2sMilwaukee%2C+WI%2C+USA!5e0!3m2!1sen!2sin!4v1461047513765" width="450" height="200" frameborder="0" style="border:0" allowfullscreen=""></iframe>

                        </div>
                    </div>
                </div>



<!-- end of body -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

<!-- footer -->
 @include('includes.admin-foot') 