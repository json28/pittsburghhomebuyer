<style type="text/css">
    
    .ellipsis {
    max-width: 80px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}
</style>


<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<div align="right">
<!-- <button type="button" class="btn btn-primary">Add Testimonials</button> -->
 <a class="btn btn-primary" href="{{ route('testimonialsmain.create') }}"> Create New Item</a>
</div><br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
<!-- body here -->
<div class="panel panel-default">
                        <div class="panel-heading">
                            Testimonials
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                            
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th max-width="120">Content</th>
                                            <th>Autor</th>
                                            <th>Page</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach ($items as $key => $item)
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->title }}</td>
                                            <td class="ellipsis">{{ strip_tags($item->content) }}</td>
                                            <td>{{ $item->autor }}</td>
                                            <td>{{ $item->page }}</td>
                                            <td>
                                            <a class="btn btn-primary" href="{{ route('testimonialsmain.edit',$item->id) }}">Edit</a>
                                            {!! Form::open(['method' => 'DELETE','route' => ['testimonialsmain.destroy', $item->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                            </td>
                                        </tr>
                                @endforeach
                                    </tbody>
                                </table>
                         {!! $items->render() !!}
                            </div>
                        </div>
                    </div>






<!-- end of body -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

<!-- footer -->
 @include('includes.admin-foot') 