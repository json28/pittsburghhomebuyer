<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<div align="right">
</div><br>


<style type="text/css">
    .cke_contents{
        height: 100px !important;
    }


</style>
<!-- body here -->

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

<label>Update Homepage</label>
{!! Form::model($item, ['method' => 'PATCH','route' => ['homecontentmain.update', $item]]) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                         Home Header Section
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Right Banner Title:</strong>
                                    {!! Form::textarea('banner_rtitle', null, array('id' => 'summary-ckeditora1','placeholder' => 'Description','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div>
                           <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Right Banner Sub Title:</strong>
                                    {!! Form::textarea('banner_rsubtitle', null, array('id' => 'summary-ckeditora2','placeholder' => 'Description','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Left Banner Title:</strong>
                                    {!! Form::textarea('banner_ltitle', null, array('id' => 'summary-ckeditora3','placeholder' => 'Description','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Left Banner Sub Title:</strong>
                                    {!! Form::textarea('banner_lsubtitle', null, array('id' => 'summary-ckeditora4','placeholder' => 'Description','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div>
                        </div>
                  </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                         Home Body Section
                        </div>
                        <div class="panel-body">
                        <div class="row">
                        

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>First Section Title:</strong>
                                    {!! Form::text('section1_title', null, array('placeholder' => 'First Section Title','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>First Section Content:</strong>
                                    {!! Form::textarea('section1_content', null, array('id' => 'summary-ckeditora5','placeholder' => 'First Section Content','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Second Section Title:</strong>
                                    {!! Form::text('section2_title', null, array('placeholder' => 'Second Section Title','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Second Section Content:</strong>
                                    {!! Form::textarea('section2_content', null, array('id' => 'summary-ckeditora6','placeholder' => 'Second Section Content','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Third Section Title:</strong>
                                    {!! Form::text('section3_title', null, array('placeholder' => 'Third Section Title','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Third Section Content:</strong>
                                    {!! Form::textarea('section3_content', null, array('id' => 'summary-ckeditora7','placeholder' => 'Third Section Content','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Splash text:</strong>
                                    {!! Form::text('spalsh_text', null, array('placeholder' => 'Splash Text','class' => 'form-control')) !!}
                                </div>
                            </div>
                           <!--  <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Fourth Section Content:</strong>
                                    {!! Form::textarea('section4_content', null, array('id' => 'summary-ckeditora8','placeholder' => 'Fourth Section Content','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div> -->
                           <!--  <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Second Splash text:</strong>
                                    {!! Form::text('spalsh2_text', null, array('placeholder' => 'Second Splash Text','class' => 'form-control')) !!}
                                </div>
                            </div> -->
                        </div>
                  </div>
            </div>



                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

   {!! Form::close() !!}

            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
<script type="text/javascript" src="{!! asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}"></script>
<!-- <script src="{{ asset('') }}"></script> -->
<script>
    CKEDITOR.replace( 'summary-ckeditora1' );
    CKEDITOR.replace( 'summary-ckeditora2' );
    CKEDITOR.replace( 'summary-ckeditora3' );
    CKEDITOR.replace( 'summary-ckeditora4' );
    CKEDITOR.replace( 'summary-ckeditora5' );
    CKEDITOR.replace( 'summary-ckeditora6' );
    CKEDITOR.replace( 'summary-ckeditora7' );
    CKEDITOR.replace( 'summary-ckeditora8' );
    CKEDITOR.replace( 'summary-ckeditora9' );
</script>
<!-- footer -->
 @include('includes.admin-foot') 