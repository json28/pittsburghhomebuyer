<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<div align="right">
<!-- <button type="button" class="btn btn-primary">Add Testimonials</button> -->
 <!-- <a class="btn btn-primary" href="{{ route('testimonialsmain.create') }}"> Create New Item</a> -->
</div><br>

<!-- body here -->
<div class="panel panel-default">
                        <div class="panel-heading">
                            Testimonials
                        </div>
                        <div class="panel-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            {!! Form::open(array('route' => 'testimonialsmain.store','method'=>'POST')) !!}
                    <div class="row">


                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Title:</strong>
                                {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Page:</strong>
                               <!--  {!! Form::text('page', null, array('placeholder' => 'Title','class' => 'form-control')) !!} -->
                               <select class="form-control" id="page" name="page">
                                  <option value="home">Home</option>
                                  <option value="about">About Us</option>
                                  <option value="service">Our Service</option>
                                  <option value="works">How it works</option>
                                  <option value="recent">Recent Project</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Author:</strong>
                                {!! Form::text('autor', null, array('placeholder' => 'Author Name','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Content:</strong>
                              <!--   {!! Form::textarea('content', null, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!} -->
                                <textarea class="form-control" id="summary-ckeditor" name="content"></textarea>

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                        </div>


                    </div>
        {!! Form::close() !!}
                    </div>






<!-- end of body -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
<script type="text/javascript" src="{!! asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}"></script>
<!-- <script src="{{ asset('') }}"></script> -->
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
<!-- footer -->
 @include('includes.admin-foot') 