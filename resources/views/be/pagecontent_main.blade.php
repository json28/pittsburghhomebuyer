<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

<div align="right">
<!-- <button type="button" class="btn btn-primary">Add Testimonials</button> -->
 <!-- <a class="btn btn-primary" href="{{ route('testimonialsmain.create') }}"> Create New Item</a> -->
</div><br>

<!-- body here -->

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

<label>Update {{ $item->page }}</label>

            {!! Form::model($item, ['method' => 'PATCH','route' => ['pagescontentmain.update', $item->id]]) !!}
<div class="panel panel-default">
            <div class="panel-heading">
                         Header Section
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Header Title:</strong>
                                    {!! Form::text('header_title', null, array('placeholder' => 'Header Title','class' => 'form-control')) !!}
                                </div>
                            </div>
                             <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Header Sub Title:</strong>
                                    {!! Form::text('header_subtitle', null, array('placeholder' => 'Header Sub Title','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Header Question Title:</strong>
                                    {!! Form::text('question_headertitle', null, array('placeholder' => 'Header Question Title','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Header Question SubTitle:</strong>
                                    {!! Form::text('question_headersubtitle', null, array('placeholder' => 'Header Question SubTitle','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>First Question Content:</strong>
                                    {!! Form::textarea('question_content1', null, array('id' => 'summary-ckeditor2','placeholder' => 'Description','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Second Question Content:</strong>
                                    {!! Form::textarea('question_content2', null, array('id' => 'summary-ckeditor3','placeholder' => 'Description','class' => 'form-control','style'=>'height:50px')) !!}
                                </div>
                            </div>
                            
                            
                        </div>
                 
                  </div>

            </div>

            <div class="panel panel-default">
            <div class="panel-heading">
                         Body Section
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Content:</strong>
                                    {!! Form::textarea('content_main', null, array('id' => 'summary-ckeditor','placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Secondary Content:</strong>
                                    {!! Form::textarea('content_main2', null, array('id' => 'summary-ckeditor4','placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
                                </div>
                            </div>

                        </div>
                 
                  </div>

            </div>



                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

   {!! Form::close() !!}

            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
<script type="text/javascript" src="{!! asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}"></script>
<!-- <script src="{{ asset('') }}"></script> -->
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
    CKEDITOR.replace( 'summary-ckeditor2' );
    CKEDITOR.replace( 'summary-ckeditor3' );
    CKEDITOR.replace( 'summary-ckeditor4' );
</script>
<!-- footer -->
 @include('includes.admin-foot') 