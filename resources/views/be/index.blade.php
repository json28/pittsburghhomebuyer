  @include('includes.admin-head') 
<body style="background-color: #E2E2E2;">
    <div class="container">
        <div class="row text-center " style="padding-top:100px;">
            <div class="col-md-12">
                <img src="img/logo_final.jpg"  width="300" />
            </div>
        </div>
         <div class="row ">
               
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                           
                            <div class="panel-body">
                                <form role="form">
                                    <hr />
                                    <h5>Enter Details to Login</h5>
                                       <br />
                                       <span id="error" style="color: red"></span>
                                     <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                            <input type="text" class="form-control" placeholder="Your Username " id="login"/>
                                        </div>
                                                                              <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                            <input type="password" class="form-control"  placeholder="Your Password" id="password"/>
                                        </div>
                                     
                                     <!-- <a class="btn btn-primary" onclick="checkLoginPass()">Login Now</a> -->
                                     <input class="btn btn-primary" type="button" id="goButton" onclick="checkLoginPass()" value="Log In"/>
                                    <hr />
                                    Not register ? <a href="index.html" >click here </a> or go to <a href="index.html">Home</a> 
                                    </form>
                            </div>
                           
                        </div>
                
                
        </div>
    </div>

</body>
</html>

<script type="text/javascript">
    var checkLoginPass = function () {
    var login = document.getElementById("login").value;
    var pass = document.getElementById("password").value;
    if (login === "admin" && pass === "admin") {
        // window.location("/admindashboard");
        location.replace("/admindashboard");
    }
    else {
        var i = "Username or Password is incorrect";
       document.getElementById("error").innerHTML = i;
    }
};
</script>