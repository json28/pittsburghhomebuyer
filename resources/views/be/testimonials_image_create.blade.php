<!-- head -->
  @include('includes.admin-head') 
    <div id="wrapper">
<!-- nav -->
      @include('includes.admin-nav') 
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
                    
<!-- body here -->
                <h1>File Upload</h1>
                 {!! Form::open(array('route' => 'testimonials-img','method'=>'POST','enctype'=>'multipart/form-data')) !!}
                    <label>Select image to upload:</label>
                    <input type="file" name="path" id="path">
                    <input type="submit" value="Upload" name="submit">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                   {!! Form::close() !!}


                <div class="col-md-12">
                        <h1 class="page-head-line">Testimonials Images</h1>
                        <!-- <h1 class="page-subhead-line">Lorem..loremm</h1> -->
                    </div>

                <!-- /. ROW  -->
                <div id="port-folio">
                    <div class="row " >
                    @foreach ($items as $key => $item)
                        <div class="col-md-4 ">

                            <div class="portfolio-item awesome mix_all" data-cat="awesome" >


                                <img src="img/{{ $item->path }}" class="img-responsive " alt="" height="300"  />
                                <div class="overlay">
                                    <p>
                                        {{ $item->page }}
                                    </p>
                                    <a class="preview btn btn-info " title="Image Title Here" href="img/{{ $item->path }}"><i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                         @endforeach
                    </div>
            </div>
        </div>
               





<!-- end of body -->
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

<!-- footer -->
 @include('includes.admin-foot') 