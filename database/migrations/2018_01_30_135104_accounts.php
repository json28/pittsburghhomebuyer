<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Accounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('accountes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone');
            $table->string('facebook');
            $table->string('instagram');
            $table->string('tweeter');
            $table->string('youtube');
            $table->string('gmail');
            $table->text('gmap');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("accountes");
    }
}
