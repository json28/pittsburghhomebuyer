<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Areas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slugs');
            $table->text('contentmain');
            $table->string('thumbnail');
            $table->text('option1_content');
            $table->string('option1_thumbnail');
            $table->text('option2_content');
            $table->string('option2_thumbnail');
            $table->text('option3_content');
            $table->string('option3_thumbnail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("areas");
    }
}
